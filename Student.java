
public class Student {
	int studentID;
	String studentName;
	String departmentName;
	int semesterNo;
	public Student(int studentID, String studentName, String departmentName, int semesterNo) {
		this.studentID = studentID;
		this.studentName = studentName;
		this.departmentName = departmentName;
		this.semesterNo = semesterNo;
	}
	void display()
	{
	System.out.println("Student ID : "+studentID+"\n");
	System.out.println("Name of the Student : "+studentName+"\n");
	System.out.println("Department Name : "+departmentName+"\n");
	System.out.println("Semester Number : "+semesterNo);
	}
	
}
