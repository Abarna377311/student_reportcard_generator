import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainClass {

	boolean validateInputs(int studentID,int semNumber,int total_subjects,int points_earned)
	{
		boolean val;
		//if the studentId is not of length 4 or if semester no and total_subjects are not within range [1 to 7] or if pointsearned is not within range [1 to 20]
		if((String.valueOf(studentID).length()!=4)||(semNumber<1||semNumber>7)||(total_subjects<1||total_subjects>7)||(points_earned<=0||points_earned>20))
		{
			val=false;
		}
		else
			val=true;
		return val;
		
	}
	public static void main(String args[]) throws NumberFormatException, IOException
	{
		int studentID,semesterNo,total_subjects,points_earned;
		String studentName, departmentName;
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the student ID of length strictly to 4: ");
		studentID=Integer.parseInt(br.readLine());
		System.out.println("Enter the student Name : ");
		studentName=br.readLine();
		System.out.println("Enter the Department Name : ");
		departmentName=br.readLine();
		System.out.println("Enter the Semester Number ranges from [1 to 7]:");
		semesterNo=Integer.parseInt(br.readLine());
		System.out.println("Enter the Total Subjects ranges from [1 to 7]: ");
		total_subjects=Integer.parseInt(br.readLine());
		System.out.println("Enter the Points Earned ranges from [1 to 20]: ");
		points_earned=Integer.parseInt(br.readLine());
		MainClass mc=new MainClass();
		if(mc.validateInputs(studentID, semesterNo, total_subjects, points_earned)==true)
		{
			StudentReportCard src=new StudentReportCard(studentID,studentName,departmentName,semesterNo,total_subjects,points_earned);
			//Calling the cgpaCalculator method to calculate the cgpa passing the gpaCalculator mathod as parameter thereby calculating the gpa as well
			src.cgpaCalculator(src.gpaCalculator());
			//Finally dispalying the all the inputs and the cgpa calculated
			src.display();
		}
		else
			System.out.println("Invalid Request");
		
	}
}
