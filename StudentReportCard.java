
public class StudentReportCard extends Student{

	int total_subjects;
	int points_earned;
	float gpa=0;
	float cgpa=0;
	StudentReportCard(int studentID,String studentName,String departmentName,int semesterNo,int total_subjects,int points_earned)
	{
		//Inheriting the super class constructor from Student Class
		super(studentID,studentName,departmentName,semesterNo);
		this.total_subjects=total_subjects;
		this.points_earned=points_earned;
	}
	public float gpaCalculator()
	{
		gpa=(total_subjects*points_earned)/7;
		return gpa;
	}
	float cgpaCalculator(float gpa)
	{
		cgpa=this.gpa/super.semesterNo;
		return cgpa;
	}
	public void display()
	{
		//Inheriting the super class method from Student Class 
		super.display();
		System.out.println("\nGPA computed : "+gpa+"\n\nCGPA computed : "+cgpa);
	}
}
